import random

name = 'radek'
x = 0

def check_name(name):
    user_name = input('Podaj Imie: ')
    if name == user_name.lower():
        print("Witaj, mój imienniku! ")
    else:
        print(f"Witaj {user_name}! ")

def is_zero():
    number = int(input('Podaj liczbe: '))
    if number:
        print(number)
    else:
        print("some string")

class Cube:
    def __init__(self, a):
        self.a = a
        self.volume = a*a*a

    def __eq__(self, other):
        return self.volume == other.volume

    def __lt__(self, other):
        return self.volume < other.volume

    def __gt__(self, other):
        return self.volume > other.volume

    def __le__(self, other):
        return self.volume <= other.volume

    def __ge__(self, other):
        return self.volume >= other.volume

    def __ne__(self, other):
        return self.volume != other.volume

class Square:
    def __init__(self, a):
        self.a = a
        self.area = a*a

    def __eq__(self, other):
        return self.area == other.area

    def __lt__(self, other):
        return self.area < other.area

    def __gt__(self, other):
        return self.area > other.area

    def __le__(self, other):
        return self.area <= other.area

    def __ge__(self, other):
        return self.area >= other.area

    def __ne__(self, other):
        return self.area != other.area


class LivingCreatures:
    def __init__(self, age):
        self.age = age

    def __eq__(self, other):
        return self.age == other.age

    def __lt__(self, other):
        return self.age < other.age

    def __gt__(self, other):
        return self.age > other.age

    def __le__(self, other):
        return self.age <= other.age

    def __ge__(self, other):
        return self.age >= other.age

    def __ne__(self, other):
        return self.age != other.age


class Mammal(LivingCreatures):
    def __init__(self, age, leg_numb):
        super().__init__(age)
        self.leg_numb = leg_numb

    def __eq__(self, other):
        return super().__eq__(other) and (self.leg_numb == other.leg_numb)

    def __lt__(self, other):
        return super().__lt__(other) and (self.leg_numb < other.leg_numb)

    def __gt__(self, other):
        return super().__gt__(other) and (self.leg_numb > other.leg_numb)

    def __le__(self, other):
        return super().__le__(other) and (self.leg_numb <= other.leg_numb)

    def __ge__(self, other):
        return super().__ge__(other) and (self.leg_numb >= other.leg_numb)

    def __ne__(self, other):
        return super().__ne__(other) and (self.leg_numb != other.leg_numb)

class Dog(Mammal):
    def __init__(self, age, leg_numb, breed):
        super().__init__(age, leg_numb)
        self.breed = breed

    def __eq__(self, other):
        return super().__eq__(other) and (self.breed == other.breed)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other) and (self.breed != other.breed)

class Cat(Mammal):
    def __init__(self, age, leg_numb, name):
        super().__init__(age, leg_numb)
        self.name = name

    def __eq__(self, other):
        return super().__eq__(other) and (self.name == other.name)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other) and (self.name != other.name)

class Horse(Mammal):
    def __init__(self, age, leg_numb, coat_color):
        super().__init__(age, leg_numb)
        self.coat_color = coat_color

    def __eq__(self, other):
        return super().__eq__(other) and (self.coat_color == other.coat_color)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other) and (self.coat_color != other.coat_color)


class Bird(LivingCreatures):
    def __init__(self, age, weight):
        super().__init__(age)
        self.weight = weight

    def __eq__(self, other):
        return super().__eq__(other) and (self.weight == other.weight)

    def __lt__(self, other):
        return super().__lt__(other) and (self.weight < other.weight)

    def __gt__(self, other):
        return super().__gt__(other) and (self.weight > other.weight)

    def __le__(self, other):
        return super().__le__(other) and (self.weight <= other.weight)

    def __ge__(self, other):
        return super().__ge__(other) and (self.weight >= other.weight)

    def __ne__(self, other):
        return super().__ne__(other) and (self.weight != other.weight)

class Amphibian(LivingCreatures):
    def __init__(self, age, genre):
        super().__init__(age)
        self.genre = genre

    def __eq__(self, other):
        return super().__eq__(other) and (self.genre == other.genre)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other) and (self.genre != other.genre)

while x != 20:
    x = random.randrange(0, 40, 2)

print(x)

for i in range(20, -1, -1):
    print(i)


